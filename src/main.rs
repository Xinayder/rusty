extern crate bufstream;

use std::io::prelude::*;
use std::io::BufReader;
use std::net::TcpStream;
use std::str::FromStr;
use bufstream::BufStream;

struct IrcMessage {
    raw_message: String,
    prefix: String,
    command: String,
    params: Vec<String>
}

impl IrcMessage {
    fn new(raw_msg: &str) -> Self {
        let mut raw: &str = raw_msg;
        let mut new_msg = IrcMessage {
            raw_message: String::new(),
            prefix: String::new(),
            command: String::new(),
            params: Vec::with_capacity(15)
        };

        new_msg.raw_message = String::from_str(raw).unwrap();
        if raw.starts_with(":") {
            let first_whitespace: u32 = match raw.find(' ') {
                Some(x) => x as u32,
                None => 0u32
            };
            new_msg.prefix = String::from_str(raw.substr(1, first_whitespace - 1)).unwrap();
            raw = raw.substr(first_whitespace + 1, raw.len() as u32 - (first_whitespace+1));
        }
        
        if raw.contains(' ') {
            let space_index = match raw.find(' ') {
                Some(x) => x as u32,
                None => 0u32,
            };
            new_msg.command = String::from(raw.substr(0, space_index));
            raw = raw.substr(space_index + 1, raw.len() as u32 - (space_index + 1));

            // Parse parameters
            let mut parameters: Vec<String> = Vec::new();
            while raw != "" {
                if raw.starts_with(":") { 
                    parameters.push(String::from(raw.substr(1, raw.len() as u32 - 1)));
                    break;
                }

                if !raw.contains(' ') {
                    parameters.push(String::from(raw));
                    raw = "";
                    break;
                }
                let space_index = match raw.find(' ') {
                    Some(x) => x as u32,
                    None => 0u32
                };
                parameters.push(String::from(raw.substr(0, space_index)));
                raw = raw.substr(space_index + 1, raw.len() as u32 - (space_index + 1));
            }
            new_msg.params = parameters;
        }

        return new_msg;
    }
}

struct IrcUser {
    nick: String,
    user: String,
    real_name: String,
    ns_passwd: String
}

impl IrcUser {
    fn new(nick: &str, user: &str, real_name: &str, ns_passwd: &str) -> Self {
        IrcUser {
            nick: String::from(nick),
            user: String::from(user),
            real_name: String::from(real_name),
            ns_passwd: String::from(ns_passwd)
        }
    }
}

struct IrcConnection {
    stream: BufStream<TcpStream>,
    user: IrcUser
}

impl IrcConnection {
    fn new(server: &str, user: IrcUser) -> Self {
        let stream = TcpStream::connect(server).unwrap();
        IrcConnection {
            stream: BufStream::new(stream),
            user: user
        }
    }

    fn send_raw(&mut self, msg: &str) -> Result<(), std::io::Error> {
        let mut message = String::from(msg);
        message = message + "\r\n";

        try!(self.stream.write(message.as_bytes()));
        self.stream.flush();
        println!("<< {}", msg);

        Ok(())
    }

    fn join(&mut self, chan: &str) {
        let mut channel = String::from(chan);
        if !chan.starts_with("#") { channel.insert(0, '#') }

        let mut msg = String::from("JOIN ");
        msg = msg + &channel;

        self.send_raw(&msg);
    }
}

trait Substring {
    fn substr(&self, start_index: u32, length: u32) -> &str;
}

impl Substring for str {
    fn substr(&self, start_index: u32, length: u32) -> &str {
        return &self[start_index as usize .. start_index as usize + length as usize];
    }
}

fn main() {
    let user = IrcUser::new("rustybot_1337", "rusty", "rusty IRC bot", "");
    let mut conn = IrcConnection::new("irc.freenode.net:6667", user);

    conn.send_raw("NICK rustybot_1337");
    conn.send_raw("USER rusty 0 * :rusty IRC bot");
    conn.join("rockytv");

    let mut buffer = String::new();

    while conn.stream.read_line(&mut buffer).unwrap() > 0 {
        let message = IrcMessage::new(&buffer);
        println!(">> {}", message.raw_message);

        // Handle PING requests
        if message.command == "PING" {
            let mut reply = String::from("PONG :");
            reply = reply + &message.params[0];
            conn.send_raw(&reply);
        }

        buffer.clear();
    }
}
